using namespace Eigen;

/*
 * Implements Full and Economical QR Decomposition.
 * One could use the Economical One to test the
 * implementation of testQR()
 *
 */
class QRfac : HouseholderQR<MatrixXd> {
	public:
		
		// Constructor, A = QR
		QRfac(const MatrixXd &A);
		// Compuate full QR
		void compute_fullQR();
		// Compute economical QR
		void compute_ecoQR();
		// Matrix-vector Multiplication Qx
		VectorXd mvQ(const VectorXd &x) const;
		// Matrix-vector Multiplication Rx
		VectorXd mvR(const VectorXd &x) const;
		// Print QR Decomposition
		void print();
	private:
		const MatrixXd &A;
		MatrixXd Q;
		MatrixXd R;
};
