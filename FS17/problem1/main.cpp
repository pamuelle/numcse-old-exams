/* NumCSE Final Exam FS17 - Problem 1
 *
 * Author: Pascal Müller [pamuell@student.ethz.ch]
 *
 * Status: Corrected
 *
 */

#include <iostream>
#include <Eigen/Dense>

#include "QRfac.hpp"
#include "problem1.hpp"

using namespace Eigen;



int main() {
	std::cout << "Problem 1:\n";
	
	// Define matrix A
	MatrixXd A(4,2);
	A << 1,2,
	     3,4,
	     5,6,
	     7,8;

	// Create QRfac Object
	QRfac fac = QRfac(A);


	// Now we compuate a full or a eco QR

	// Calcualte the full QR-Decomposition of A
	fac.compute_fullQR();	
	//fac.print();

	// Calculate the economical QR-Decomposition of A
	//fac.compute_ecoQR();
	//fac.print();


	// Check if full QR or eco QR was used
	if(testQR(fac, A) == true) {
		std::cout << "full QR\n";
	}

	return 0;
}
