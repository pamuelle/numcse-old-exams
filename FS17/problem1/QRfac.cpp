#include<iostream>
#include <Eigen/Dense>
#include <Eigen/QR>

#include "QRfac.hpp"

using namespace Eigen;


// Constructor, A = QR
QRfac::QRfac(const MatrixXd &A) : A(A) {};

// Compuate full QR
void QRfac::compute_fullQR() {
	HouseholderQR<MatrixXd> qr(A);
	Q = qr.householderQ();
	R = qr.matrixQR().template triangularView<Upper>();
}

// Compute economical QR
void QRfac::compute_ecoQR() {
	MatrixXd::Index n = A.cols();
	MatrixXd::Index m = A.rows();
	HouseholderQR<MatrixXd> qr(A);
	Q = (qr.householderQ()*MatrixXd::Identity(m,n));
	R = qr.matrixQR().block(0,0,n,n).template triangularView<Upper>();
}

// Matrix-vector Multiplication Qx
VectorXd QRfac::mvQ(const VectorXd &x) const {return Q*x; }

// Matrix-vector Multiplication Rx
VectorXd QRfac::mvR(const VectorXd &x) const { return R*x; }

// Print QR-Decomposition
void QRfac::print() {
	std::cout << "A:\n" << A << "\nQ:\n" << Q << "\nR:\n" << R << "\n\n";
}
