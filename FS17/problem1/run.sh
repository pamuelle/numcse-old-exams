#!/bin/bash

if g++ -std=c++11 -I /usr/include/eigen3 -c QRfac.cpp problem1.cpp main.cpp && g++ QRfac.o problem1.o main.o; then
    echo Compilation successful! Now running...
    ./a.out
else
    echo Compilation error!
fi
