#include <Eigen/Dense>
#include <iostream>
#include <limits>

#include "QRfac.hpp"
#include "problem1.hpp"

using namespace Eigen;

/*
 * A: m-n Matrix
 * Q: m-m Matrix
 * R: m-n Matrix
 *
 * Properties of a Full QR:
 * 1) A=QR
 * 2) Q^T*Q=Q*Q^T=I
 * 3) R is upper triangular of size n
 *
 * Note: We don't have access to Q and R. We only
 *       have access to mvQ() and mvR().
 *
 * Idea: Build Q and R using mvQ() and mvR() by
 *		 using the canonical basis.
 * 		 Then check properties.  
 */
bool testQR(const QRfac& qrf, const MatrixXd &A) {
	size_t m = A.rows();
	size_t n = A.cols();
	MatrixXd Q(m,m);
	MatrixXd R(m,n);
	float EPS = std::numeric_limits<double>::epsilon()*10;

	VectorXd e_i;

	// Construct Q
	for(size_t i=0; i<m; ++i) {
		// Construct canonical basis vector
		e_i = VectorXd::Zero(m);
		e_i(i) = 1.0;
		
		// Get i-th columns
		Q.col(i) = qrf.mvQ(e_i);	
	}

	// Construct R
	for(size_t i=0; i<n; ++i) {
		// Construct canonical basis vector
		e_i = VectorXd::Zero(n);
		e_i(i) = 1.0;
		
		// Get i-th columns
		R.col(i) = qrf.mvR(e_i);

		// Check propertie 3) - Is R upper triangular?
		if(R.col(i).tail(m-i-1) != VectorXd::Zero(m-i-1)) {
			std::cout << "ERROR: R is not upper triangular.\n";
			return false;
		}	
	}

	// Check Propertie 1) A=Q*R.
	// Since we get results of floating point artihemtic
	// we must consider an error
	if( (Q*R-A).norm() > EPS*A.norm() ) {
		std::cout << "ERROR: A!=QR\n";
		return false; // A != QR
	}	
	

	// Check propertie 2) - Is Q orthogonal? Check Q^T*Q=I
	if( (Q.transpose()*Q - MatrixXd::Identity(m,m)).norm() > EPS ) {
		std::cout << "ERROR: Q is not orthogonal\n";
		return false;
	}
	
	return true;
}
