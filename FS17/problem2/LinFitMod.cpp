#include<iostream>
#include<cassert>
#include<Eigen/Dense>

#include"LinFitMod.hpp"

using namespace Eigen;


LinFitMod::LinFitMod(const MatrixXd& X, const VectorXd& z) {
	assert(X.rows() == z.size() && "z must have same length of no. of rows of X");
	lu_m = (X.transpose()*X).fullPivLu();
	q_m = lu_m.solve(X.transpose()*z);
}
	
VectorXd LinFitMod::solve(const VectorXd& xm, double ym) {
	VectorXd a;

	VectorXd p = lu_m.solve(xm);
	VectorXd w = q_m + ym * p;
	double xi = xm.dot(p);
	
	return w - xm.dot(w) / (1.0 + xi) * p;
}
