#!/bin/bash

if g++ -std=c++11 -I /usr/include/eigen3 -c LinFitMod.cpp main.cpp && g++ LinFitMod.o main.o; then
    echo Compilation successful! Now running...
    ./a.out
else
    echo Compilation error!
fi
