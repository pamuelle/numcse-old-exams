/* NumCSE Final Exam FS17 - Problem 2
 *
 * Author: Pascal Müller [pamuell@student.ethz.ch]
 *
 * Status: Corrected
 *
 */

#include <iostream>
#include <Eigen/Dense>

#include"LinFitMod.hpp"

using namespace Eigen;

int main() {
	std::cout << "Problem 2:\n";
	
	/* I'm not sure if I fully understand this exercise.
	 * Those tests are written by myself and I have no
	 * knwoledge of how it was implemented in the exam.
	 *
	 * Author: Pascal Müller [pamuelle@student.ethz.ch]
	 *
	 */

	// TEST 1: Linear Regression through the points:
	//		   (0,0), (1,0), (0,1)
	// Whereas we add the points (1,1) and (2,3)
	//
	// We use the model y = alpha*x + beta.
	// See example 3.0.1.1
	//
	// We always fit data against a model which we
	// get from the theory. E.g. we make a physics
	// experiment and the physics theory might tell
	// use we expect a linear relationship - so we'd
	// use a linear model. We could also use a quadratic
	// model (ax*bx^2*c) if we would expect such a relationship.
	
	// Make linear regression for (0,0), (1,0), (0,1)
	// Model: y=alpha*x + beta
	MatrixXd X(3,2); // (x,y)
	X << 0,1, // (0,0)
		 1,1, // (1,0)
		 0,1; // (0,1)
	VectorXd y(3);
	y << 0, // (0,0)
		 0, // (1,0)
		 1;	// (0,1)

	// Construct object
	LinFitMod lfm = LinFitMod(X,y);

	// Add Point (1,1)
	VectorXd xm(2);
	xm << 1,1;
	double ym = 1;
	// Solve it for added point (xm,ym)=(1,1)
	VectorXd a = lfm.solve(xm,ym);
	// Expected output: 0, 0.5
	std::cout << "Add (1,1) -> \n" << a << "\n\n";

	
	// We can add another point - but the way LinFitMod::solve()
	// Is implemented, we kind of overwrite the last added point.
	// So if we add e.g. (2,4) we make a linear regression for
	// the points (0,0), (1,0), (0,1), (2,4) - without (1,1)
	// See https://www.desmos.com/calculator/jwquvmikhr

	// Add (2,4)
	xm << 2,1;
	ym = 4;	
	// Solve it for added point (xm,ym)=(2,4)
	a = lfm.solve(xm,ym);
	// Expected output: 1.4545, 0.0909091
	std::cout << "Add (2,4) -> \n" << a << "\n\n";

	return 0;
}
