using namespace Eigen;

class LinFitMod {
	public:
		LinFitMod(const MatrixXd& X, const VectorXd& z);
		VectorXd solve(const VectorXd& xm, double ym);
	private:
		FullPivLU<MatrixXd> lu_m;
		VectorXd q_m;
};
