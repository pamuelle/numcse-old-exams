# NumCSE Old Exams

Personal Repository used to solve old exams of NumCSE (Hiptmair).

Each old exam has it's own folder in which I solved it. Later on, I might create templates out of my solution for others - we'll see.

The Resources folder was copied from a virtual machine we got (NuMCSE HS19) which was a showcase of the exam situation. Note that the lecutre document changes all the time so references in older and future exams might not be 100% correct
